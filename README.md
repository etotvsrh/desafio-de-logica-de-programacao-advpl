# DESAFIO ADVPL 1 #

A área de Desenvolvimento do RH Protheus da TOTVS está realizando um experimento para inovar nas formas de recrutamento e seleção da área. A TOTVS criou um ambiente Protheus online que pode ser utilizado para que qualquer usuário possa desenvolver e testar seus algorítimos em ADVPL. Se prepare e esteja pronto para participar dos desafios de lógica em ADVPL!

Resolva os 3 desafios, dê um "Pull Request" de seus códigos e boa sorte!

### Link da Ferramenta de Teste ###

http://vmctb67197:8080/h_editor.apl

### Primeiro Desafio ###

Dado um array de Inteiros, Você pode encontrar a soma dos elementos?


Formato de entrada:

A primeira linha contém um inteiro que se refere a quantidade de elementos do array (tamanho do array). 
A segunda linha contém os itens separados por um espaço, representando os elementos do array.

Exemplo de entrada:      
6     
1 2 3 4 10 11

Formato de saída:
Imprima o resultado da soma de todos os elementos do array em um número inteiro.

Exemplo de saída:
31


Explicação:
A soma dos valores 1 + 2 + 3 + 4 + 10 + 11 é 31.


### Segundo Desafio ###

Considere a escada de tamanho 4:

---T    
--TT   
-TTT   
TTTT

Observe que a sua base e sua altura são ambas iguais a N e a imagem é desenhada usando 'T' e espaços. A última linha não é precedida de nenhum espaço. Escreva um programa que imprima uma escada de tamanho N.

Formato entrada:
Um número inteiro,N, Que representará o tamanho.

Exemplo de entrada:
6 

Exemplo de saída:



-----T   
----TT   
---TTT   
--TTTT    
-TTTTT    
TTTTTT



Explicação
A escada é alinhada a direita e composta por 'T' e espaços, de largura e tamanhos iguais a N=6.


### Terceiro Desafio ###

Faça um algoritmo em ADVPL que peça 2 números ao usuário (sendo o segundo maior que o primeiro) e mostre nesse “espaço” de números quais são números primos.

Exemplo: O usuário digitou 6 e 10 (6 não é primo, 7 É, 8 não é, 9 não é, 10 não é... então só vai mostrar o 7).